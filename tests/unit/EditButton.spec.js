import EditButton from '@/components/EditButton.vue'
import { createWrapper } from '../test_utils'

describe('Edit button', () => {
  it('displays a custom label', () => {
    const label = 'a button label'
    const wrapper = createWrapper(EditButton, { label })

    expect(wrapper.text()).toMatch(label)
  })

  it('can be clicked with a item id', () => {
    const itemId = 1
    const wrapper = createWrapper(EditButton, { itemId })
    const button = wrapper.find('.v-btn')

    button.trigger('click')

    expect(wrapper.emitted('editClicked')).toBeTruthy()
    expect(wrapper.emitted('editClicked')[0]).toEqual([itemId])
  })
})
