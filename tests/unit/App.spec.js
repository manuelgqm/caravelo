import App from '@/App'
import {
  createWrapper,
  loadFlightsSuccessfulMock,
  flightQuotaSuccessfulMock,
  flightQuotaChangeReasonsSuccessfulMock,
  loadFlightsFailedMock,
  saveFlightsQuotaFailedMock
} from '../test_utils'

describe('App', () => {
  let wrapper

  beforeEach(() => {
    wrapper = createWrapper(App)
  })

  describe('when page is loaded', () => {
    it('renders "EDIT FLIGHT" button ', () => {
      const editFlightsButton = wrapper.findComponent({ ref: 'editFlightsButton' })

      expect(editFlightsButton.isVisible()).toBeTruthy()
    })

    it("didn't shows 'edit flights dialog'", () => {
      const editFlightsDialog = wrapper.findComponent({ ref: 'editFlightsDialog' })

      expect(editFlightsDialog.exists()).toBeFalsy()
    })

    it("didn't shows alerts", () => {
      const saveSuccessfulAlert = wrapper.find('[aria-label="saveSuccessfulAlert"]')
      const saveFailedAlert = wrapper.find('[aria-label="saveFailedAlert"]')
      const saveLoadAlert = wrapper.find('[aria-label="loadFailedAlert"]')

      expect(saveSuccessfulAlert.isVisible()).toBeFalsy()
      expect(saveFailedAlert.isVisible()).toBeFalsy()
      expect(saveLoadAlert.isVisible()).toBeFalsy()
    })
  })

  describe('when editing flights', () => {
    let loadFlightsMock
    let wrapper
    const flightsNumber = 5

    beforeEach(async () => {
      loadFlightsMock = loadFlightsSuccessfulMock(flightsNumber)
      flightQuotaSuccessfulMock()
      flightQuotaChangeReasonsSuccessfulMock()

      wrapper = await createWrapper(App)

      // workaround needed to avoid test warning due to vuetify issue https://forum.vuejs.org/t/vuetify-data-app-true-and-problems-rendering-v-dialog-in-unit-tests/27495/15
      wrapper.element.ownerDocument.body.setAttribute('data-app', true)

      const editFlightsButton = wrapper.findComponent({ ref: 'editFlightsButton' }).find('button')
      await editFlightsButton.trigger('click')
    })

    afterEach(() => {
      jest.clearAllMocks()
    })

    it('loads the subscriber flights number', () => {
      expect(loadFlightsMock).toHaveBeenCalledTimes(1)
      expect(wrapper.vm.editedFlightsQuota).toEqual(flightsNumber)
    })

    it('shows "edit flight" dialog when "EDIT FLIGHTS" button clicked', () => {
      const editFlightsDialog = wrapper.findComponent({ ref: 'editFlightsDialog' })
      expect(editFlightsDialog.exists()).toBeTruthy()
    })

    it('closes "edit flight" dialog when close button clicked', async () => {
      const editFlightsDialog = wrapper.findComponent({ ref: 'editFlightsDialog' })
      const dialogCloseButton = editFlightsDialog.find('[aria-label="closeDialog"]')
      await dialogCloseButton.trigger('click')

      expect(editFlightsDialog.exists()).toBeFalsy()
    })

    it('shows successful save alert when modifications are saved', async () => {
      await wrapper.vm.$refs.editFlightsDialog.$emit('saveSuccessful')
      const saveSuccessfulAlert = wrapper.find('[aria-label="saveSuccessfulAlert"]')

      expect(saveSuccessfulAlert.isVisible()).toBeTruthy()
    })
  })

  describe('when subscribers flights load fails', () => {
    let wrapper

    beforeEach(async () => {
      loadFlightsFailedMock()

      wrapper = await createWrapper(App)

      const editFlightsButton = wrapper.findComponent({ ref: 'editFlightsButton' }).find('button')
      await editFlightsButton.trigger('click')
    })

    it('shows load error alert', () => {
      const loadFailAlert = wrapper.find('[aria-label="loadFailedAlert"]')

      expect(loadFailAlert.isVisible()).toBeTruthy()
    })
  })

  describe('when fligths quota save fails', () => {
    let wrapper

    beforeEach(async () => {
      loadFlightsSuccessfulMock()
      flightQuotaSuccessfulMock()
      flightQuotaChangeReasonsSuccessfulMock()

      wrapper = await createWrapper(App)

      const editFlightsButton = wrapper.findComponent({ ref: 'editFlightsButton' }).find('button')
      await editFlightsButton.trigger('click')
    })

    it('shows save failed error alert', async () => {
      await wrapper.vm.$refs.editFlightsDialog.$emit('saveFailed')
      const saveFailedAlert = wrapper.find('[aria-label="saveFailedAlert"]')

      expect(saveFailedAlert.isVisible()).toBeTruthy()
    })
  })
})
