import CounterHandler from '@/components/CounterHandler'
import { createWrapper } from '../test_utils'

describe('Counter Handler', () => {
  let wrapper

  beforeEach(() => {
    wrapper = createWrapper(CounterHandler)
  })

  it('displays actual value', () => {
    const defaultActualCounter = '0'
    const counterContainerText = wrapper.find('#counterContainer').text()

    expect(counterContainerText).toEqual(defaultActualCounter)
  })

  it('increases counter when increment counter button clicked', async () => {
    const increaseCounterButton = wrapper.find('.v-input__append-outer button')
    await increaseCounterButton.trigger('click')
    const counterContainerText = wrapper.find('#counterContainer').text()

    expect(counterContainerText).toEqual('1')
  })

  it('reduces counter when decrement counter button clicked', async () => {
    const actualValue = 1
    wrapper = createWrapper(CounterHandler, { actualValue })

    const reduceCounterButton = wrapper.find('.v-input__prepend-outer button')
    await reduceCounterButton.trigger('click')
    const counterContainerText = wrapper.find('#counterContainer').text()

    expect(counterContainerText).toEqual('0')
  })

  describe('when trying to modify beyond limits', () => {
    it("doesn't increment over maximum", async () => {
      wrapper = createWrapper(CounterHandler, {
        maximum: 1,
        actualValue: 0
      })

      const increaseCounterButton = wrapper.find('.v-input__append-outer button')
      await increaseCounterButton.trigger('click')
      await increaseCounterButton.trigger('click')
      const counterContainerText = wrapper.find('#counterContainer').text()

      expect(counterContainerText).toEqual('1')
    })

    it("doesn't reduce under minimum", async () => {
      wrapper = createWrapper(CounterHandler, {
        minimum: 0,
        actualValue: 1
      })

      const reduceCounterButton = wrapper.find('.v-input__prepend-outer button')
      await reduceCounterButton.trigger('click')
      await reduceCounterButton.trigger('click')
      const counterContainerText = wrapper.find('#counterContainer').text()

      expect(counterContainerText).toEqual('0')
    })
  })
})
