import Vue from 'vue'
import Vuetify from 'vuetify'
import { createLocalVue, mount } from '@vue/test-utils'
import { jest } from '@jest/globals'

import SubscribersRepository from '@/services/SubscribersRepository'
import DomainRulesRepository from '@/services/DomainRulesRepository'

Vue.use(Vuetify)
const vuetify = new Vuetify()
const localVue = createLocalVue()

const createWrapper = (component, propsData) => {
  return mount(component, { localVue, vuetify, propsData })
}

const loadFlightsSuccessfulMock = (data) => (
  jest
    .spyOn(SubscribersRepository.prototype, 'loadFlights')
    .mockResolvedValue({
      failed: false,
      ok: true,
      data
    })
)

const loadFlightsFailedMock = () => (
  jest
    .spyOn(SubscribersRepository.prototype, 'loadFlights')
    .mockResolvedValue({ failed: true, ok: false })
)

const flightQuotaSuccessfulMock = () => (
  jest
    .spyOn(DomainRulesRepository.prototype, 'flightQuota')
    .mockResolvedValue({
      failed: false,
      ok: true
    })
)

const flightQuotaChangeReasonsSuccessfulMock = (data) => (
  jest
    .spyOn(DomainRulesRepository.prototype, 'flightQuotaChangeReasons')
    .mockResolvedValue({
      failed: false,
      ok: true,
      data
    })
)
const saveFlightsQuotaFailedMock = () => (
  jest
    .spyOn(SubscribersRepository.prototype, 'loadFlights')
    .mockResolvedValue({ failed: true, ok: false })
)

module.exports = {
  createWrapper,
  loadFlightsSuccessfulMock,
  flightQuotaSuccessfulMock,
  flightQuotaChangeReasonsSuccessfulMock,
  loadFlightsFailedMock,
  saveFlightsQuotaFailedMock
}
