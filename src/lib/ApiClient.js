export default class ApiClient {
  constructor (baseUrl = '') {
    this.headers = { 'Content-Type': 'application/json' }
    this.baseUrl = baseUrl
    this.data = {}
  }

  async get (endpointUrl, id = '') {
    const url = this.obtainUrl(endpointUrl, id)
    const response = await fetch(url, { headers: this.headers })

    if (response.ok) {
      this.data = 'dummy data'
    }

    return {
      ok: response.ok,
      failed: !response.ok,
      data: this.data
    }
  }

  async post (endpointUrl, payload) {
    payload = { ...payload, format: 'json' }
    const url = this.obtainUrl(endpointUrl)

    const response = await fetch(url, {
      headers: this.headers,
      method: 'POST',
      body: JSON.stringify(payload)
    })

    if (response.ok) {
      this.data = 'dummy data'
    }

    return {
      ok: response.ok,
      failed: !response.ok,
      data: this.data
    }
  }

  // private

  obtainUrl (endpointUrl, id) {
    return `${this.baseUrl}/${endpointUrl}/${id}`
  }
}
