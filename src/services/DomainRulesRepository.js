import ApiClient from '@/lib/ApiClient'

export default class DomainRulesRepository {
  constructor () {
    const baseUrl = 'https://httpstat.us'
    this.api = new ApiClient(baseUrl)
  }

  async flightQuota () {
    const flightQuotaEndpoint = '200'
    const data = {
      minFlights: 1,
      maxFlights: 3,
      modifierStep: 1
    }

    const response = await this.api.get(flightQuotaEndpoint)
    response.data = data

    return response
  }

  async flightQuotaChangeReasons () {
    const flightQuotaChangeReasonsEndpoint = '200'

    const data = [{
      value: 1,
      type: 'increment',
      text: 'Subscriber canceled flight'
    }, {
      value: 2,
      type: 'increment',
      text: 'Airline canceled flight'
    }, {
      value: 3,
      type: 'increment',
      text: 'Customer compensation'
    }, {
      value: 4,
      type: 'decrement',
      text: 'Flight not redeposited after a flight cancellation'
    }, {
      value: 5,
      type: 'decrement',
      text: 'Subscriber had log in or password issues'
    }, {
      value: 6,
      type: 'decrement',
      text: 'Subscriber had issues when booking'
    }, {
      value: 7,
      type: 'decrement',
      text: 'Subscription has not renewed correctly'
    }, {
      value: 8,
      type: 'all',
      text: 'Other'
    }]

    const response = await this.api.get(flightQuotaChangeReasonsEndpoint)
    response.data = data

    return response
  }
}
