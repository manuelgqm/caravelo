import ApiClient from '@/lib/ApiClient'

export default class SubscribersRepository {
  constructor () {
    const baseUrl = 'https://httpstat.us'
    this.api = new ApiClient(baseUrl)
  }

  async loadFlights (subscriberId) {
    if (!subscriberId) return

    const loadFlightsEndpoint = '200'
    const flightsNumber = 2

    const response = await this.api.get(loadFlightsEndpoint, subscriberId)
    response.data = flightsNumber

    return response
  }

  async saveFlightsQuota (data) {
    if (!data) return

    const saveFlightsQuotaEndpoint = '200'
    const response = await this.api.post(saveFlightsQuotaEndpoint, data)

    return response
  }
}
